﻿#include <map>
#include <algorithm>

#include "ItemNotFoundException.h"
#include "MemoryAccess.h"

MemoryAccess* a;

void MemoryAccess::printAlbums() 
{
	if(m_albums.empty()) {
		throw MyException("There are no existing albums.");
	}
	std::cout << "Album list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (const Album& album: m_albums) 	{
		std::cout << std::setw(5) << "* " << album;
	}
}

//bool MemoryAccess::open()
//{
//	// create some dummy albums
//	for (int i=0; i<5; ++i) {
//		// create some dummy users
//		std::stringstream name("User_"+std::to_string(i));
//
//		User user(i, name.str());
//		createUser(user);
//
//		m_albums.push_back(createDummyAlbum(user));
//	}
//
//	return true;
//}

void MemoryAccess::clear()
{
	m_users.clear();
	m_albums.clear();
}

auto MemoryAccess::getAlbumIfExists(const std::string & albumName)
{
	auto result = std::find_if(std::begin(m_albums), std::end(m_albums), [&](auto& album) { return album.getName() == albumName; });

	if (result == std::end(m_albums)) {
		throw ItemNotFoundException("Album not exists: ", albumName);
	}
	return result;

}

Album MemoryAccess::createDummyAlbum(const User& user)
{
	std::stringstream name("Album_" +std::to_string(user.getId()));

	Album album(user.getId(),name.str());

	for (int i=1; i<3; ++i)	{
		std::stringstream picName("Picture_" + std::to_string(i));

		Picture pic(i++, picName.str());
		pic.setPath("C:\\Pictures\\" + picName.str() + ".bmp");

		album.addPicture(pic);
	}

	return album;
}

const std::list<Album> MemoryAccess::getAlbums() 
{
	return m_albums;
}

const std::list<Album> MemoryAccess::getAlbumsOfUser(const User& user) 
{	
	std::list<Album> albumsOfUser;
	for (const auto& album: m_albums) {
		if (album.getOwnerId() == user.getId()) {
			albumsOfUser.push_back(album);
		}
	}
	return albumsOfUser;
}

void MemoryAccess::createAlbum(const Album& album)
{
	char* errM;
	char* n = new char[5];
	std::string dbFileName = "GalleryDB.sqlite";

	_itoa(album.getOwnerId(), n, 10); 

	std::string fullS = std::string("INSERT INTO Albums(Title,Created_By,Created_On) VALUES ('") + album.getName() + std::string("', ") + n + std::string(", '") + album.getCreationDate() + std::string("');");
	int res = sqlite3_exec(db, fullS.c_str(), nullptr, nullptr, &errM);
	
	//std::cout << errM << std::endl;

	m_albums.push_back(album);
}

void MemoryAccess::deleteAlbum(const std::string& albumName, int userId)
{
	std::string dbFileName = "GalleryDB.sqlite";

	std::string fullS = std::string("DELETE FROM Albums WHERE Title = '") + std::string(albumName) + std::string("';");
	int res = sqlite3_exec(db, fullS.c_str(), nullptr, nullptr, nullptr);

	for (auto iter = m_albums.begin(); iter != m_albums.end(); iter++) {
		if ( iter->getName() == albumName && iter->getOwnerId() == userId ) {
			iter = m_albums.erase(iter);
			return;
		}
	}
}

bool MemoryAccess::doesAlbumExists(const std::string& albumName, int userId) 
{
	for (const auto& album: m_albums) {
		if ( (album.getName() == albumName) && (album.getOwnerId() == userId) ) {
			return true;
		}
	}

	return false;
}

Album MemoryAccess::openAlbum(const std::string& albumName) 
{
	for (auto& album: m_albums)	{
		if (albumName == album.getName()) {
			return album;
		}
	}
	throw MyException("No album with name " + albumName + " exists");
}

void MemoryAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture) 
{
	auto result = getAlbumIfExists(albumName);

	char* errM;
	std::string dbFileName = "GalleryDB.sqlite";

	std::string fullS = std::string("INSERT INTO Pictures(Pic_Name,Pic_Path,In_Album) VALUES ('") + picture.getName() + std::string("', '") + picture.getPath() + std::string("', '") + albumName + std::string("');");

	int res = sqlite3_exec(db, fullS.c_str(), nullptr, nullptr, &errM);

	if (res != SQLITE_OK)
	{
		std::cout << "Problem - " << errM << std::endl;
	}

	(*result).addPicture(picture);
}

void MemoryAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) 
{
	auto result = getAlbumIfExists(albumName);

	char* errM;
	std::string dbFileName = "GalleryDB.sqlite";

	std::string fullS = std::string("DELETE FROM Pictures WHERE Pictures.Pic_Name='") + pictureName + std::string("';");

	int res = sqlite3_exec(db, fullS.c_str(), nullptr, nullptr, &errM);

	if (res != SQLITE_OK)
	{
		std::cout << "Problem - " << errM << std::endl;
	}

	(*result).removePicture(pictureName);
}

void MemoryAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	auto result = getAlbumIfExists(albumName);

	(*result).tagUserInPicture(userId, pictureName);
}

void MemoryAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	auto result = getAlbumIfExists(albumName);

	(*result).untagUserInPicture(userId, pictureName);
}

void MemoryAccess::closeAlbum(Album& ) 
{
	// basically here we would like to delete the allocated memory we got from openAlbum
}

// ******************* User ******************* 
void MemoryAccess::printUsers()
{
	std::cout << "Users list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (const auto& user: m_users) {
		std::cout << user << std::endl;
	}
}

User MemoryAccess::getUser(int userId) {
	for (const auto& user : m_users) {
		if (user.getId() == userId) {
			return user;
		}
	}

	throw ItemNotFoundException("User", userId);
}

void MemoryAccess::createUser(User& user)
{
	char* errM;
	std::string dbFileName = "GalleryDB.sqlite";
	
	user.setId(a->m_users.size() + 1);

	std::string fullS = std::string("INSERT INTO Users(name) VALUES ('") + user.getName() + std::string("');");

	//std::cout << fullS << std::endl;

	int res = sqlite3_exec(db, fullS.c_str(), nullptr, nullptr, &errM);

	if (res != SQLITE_OK)
	{
		std::cout << "Problem - " << errM << std::endl;
	}

	m_users.push_back(user);
}

void MemoryAccess::deleteUser(const User& user)
{
	std::string dbFileName = "GalleryDB.sqlite";

	std::string fullS = std::string("DELETE FROM Users WHERE name = '") + user.getName() + std::string("';");
	int res = sqlite3_exec(db, fullS.c_str(), nullptr, nullptr, nullptr);

	if (doesUserExists(user.getId())) {
	
		for (auto iter = m_users.begin(); iter != m_users.end(); ++iter) {
			if (*iter == user) {
				iter = m_users.erase(iter);
				return;
			}
		}
	}
}

bool MemoryAccess::doesUserExists(int userId) 
{
	auto iter = m_users.begin();
	for (const auto& user : m_users) {
		if (user.getId() == userId) {
			return true;
		}
	}
	
	return false;
}


// user statistics
int MemoryAccess::countAlbumsOwnedOfUser(const User& user) 
{
	int albumsCount = 0;

	for (const auto& album: m_albums) {
		if (album.getOwnerId() == user.getId()) {
			++albumsCount;
		}
	}

	return albumsCount;
}

int MemoryAccess::countAlbumsTaggedOfUser(const User& user) 
{
	int albumsCount = 0;

	for (const auto& album: m_albums) {
		const std::list<Picture>& pics = album.getPictures();
		
		for (const auto& picture: pics)	{
			if (picture.isUserTagged(user))	{
				albumsCount++;
				break;
			}
		}
	}

	return albumsCount;
}

int MemoryAccess::countTagsOfUser(const User& user) 
{
	int tagsCount = 0;

	for (const auto& album: m_albums) {
		const std::list<Picture>& pics = album.getPictures();
		
		for (const auto& picture: pics)	{
			if (picture.isUserTagged(user))	{
				tagsCount++;
			}
		}
	}

	return tagsCount;
}

float MemoryAccess::averageTagsPerAlbumOfUser(const User& user) 
{
	int albumsTaggedCount = countAlbumsTaggedOfUser(user);

	if ( 0 == albumsTaggedCount ) {
		return 0;
	}

	return static_cast<float>(countTagsOfUser(user)) / albumsTaggedCount;
}

User MemoryAccess::getTopTaggedUser()
{
	std::map<int, int> userTagsCountMap;

	auto albumsIter = m_albums.begin();
	for (const auto& album: m_albums) {
		for (const auto& picture: album.getPictures()) {
			
			const std::set<int>& userTags = picture.getUserTags();
			for (const auto& user: userTags ) {
				//As map creates default constructed values, 
				//users which we haven't yet encountered will start from 0
				userTagsCountMap[user]++;
			}
		}
	}

	if (userTagsCountMap.size() == 0) {
		throw MyException("There isn't any tagged user.");
	}

	int topTaggedUser = -1;
	int currentMax = -1;
	for (auto entry: userTagsCountMap) {
		if (entry.second < currentMax) {
			continue;
		}

		topTaggedUser = entry.first;
		currentMax = entry.second;
	}

	if ( -1 == topTaggedUser ) {
		throw MyException("Failed to find most tagged user");
	}

	return getUser(topTaggedUser);
}

Picture MemoryAccess::getTopTaggedPicture()
{
	int currentMax = -1;
	const Picture* mostTaggedPic = nullptr;
	for (const auto& album: m_albums) {
		for (const Picture& picture: album.getPictures()) {
			int tagsCount = picture.getTagsCount();
			if (tagsCount == 0) {
				continue;
			}

			if (tagsCount <= currentMax) {
				continue;
			}

			mostTaggedPic = &picture;
			currentMax = tagsCount;
		}
	}
	if ( nullptr == mostTaggedPic ) {
		throw MyException("There isn't any tagged picture.");
	}

	return *mostTaggedPic;	
}

std::list<Picture> MemoryAccess::getTaggedPicturesOfUser(const User& user)
{
	std::list<Picture> pictures;

	for (const auto& album: m_albums) {
		for (const auto& picture: album.getPictures()) {
			if (picture.isUserTagged(user)) {
				pictures.push_back(picture);
			}
		}
	}

	return pictures;
} 


bool MemoryAccess::open()
{
	std::string dbFileName = "GalleryDB.sqlite";
	int res = sqlite3_open(dbFileName.c_str(), &db);

	a = this;

	char* sqlStatement = "SELECT * FROM Users;";
	res = sqlite3_exec(db, sqlStatement, callbackUser, nullptr, nullptr);
	
	sqlStatement = "SELECT Albums.Created_By, Albums.Title, Albums.Created_On FROM Users INNER JOIN Albums ON Users.id = Albums.Created_By;";
	res = sqlite3_exec(db, sqlStatement, callbackAlbums, nullptr, nullptr);

	return (res == SQLITE_OK) ? true : false;
}

int MemoryAccess::callbackAlbums(void *data, int argc, char **argv, char **azColName)
{
	User* user = nullptr;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "Created_By")
		{
			user = &(a->getUser(atoi(argv[i])));
		}
		else if (std::string(azColName[i]) == "Title")
		{
			Album album((*user).getId(), argv[i]);

			a->m_albums.push_back(album);
		}
		else if (std::string(azColName[i]) == "Created_On")
		{
			a->m_albums.back().setCreationDate(argv[i]);
		}
	}

	return 0;
}

int MemoryAccess::callbackUser(void *data, int argc, char **argv, char **azColName)
{
	User* user = nullptr;
	for (int i = 0; i < argc; i++)
	{
		std::stringstream name("" + std::string(argv[i]));

		if (std::string(azColName[i]) == "id")
		{
			user = new User(atoi(argv[i]), "");
		}
		else if (std::string(azColName[i]) == "name")
		{
			user->setName(name.str());
			a->m_users.push_back(*user);
		}
	}

	return 0;
}

